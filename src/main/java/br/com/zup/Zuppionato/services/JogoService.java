package br.com.zup.Zuppionato.services;

import br.com.zup.Zuppionato.dtos.JogoDto;
import br.com.zup.Zuppionato.dtos.TimeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JogoService {
    private List<JogoDto> calendarioCampeonato = new ArrayList<>();

    @Autowired
    TimeService timeService;

    public JogoDto agendarJogo(String nome1, String nome2, JogoDto novoJogo) {
        TimeDto timeMandante = timeService.pesquisarTimePorNome(nome1);
        TimeDto timeVisitante = timeService.pesquisarTimePorNome(nome2);

        novoJogo.setTimeMandante(timeMandante);
        novoJogo.setTimeVisitante(timeVisitante);
        calendarioCampeonato.add(novoJogo);

        return novoJogo;
    }

    public List<JogoDto> mostrarCalendario() {
        return calendarioCampeonato;
    }
}

