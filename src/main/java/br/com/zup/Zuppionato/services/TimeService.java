package br.com.zup.Zuppionato.services;

import br.com.zup.Zuppionato.dtos.TimeDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimeService {
    private List<TimeDto> listaDeTimes = new ArrayList<>();

    public List<TimeDto> getListaDeTimes() {
        return listaDeTimes;
    }

    public void setListaDeTimes(List<TimeDto> listaDeTimes) {
        this.listaDeTimes = listaDeTimes;
    }

    public TimeDto cadastrarTime (TimeDto novoTime) {
        listaDeTimes.add(novoTime);

        return novoTime;
    }

    public List<TimeDto> mostrarTimes () {
        return listaDeTimes;
    }

    public TimeDto pesquisarTimePorNome (String nome) {
        for (TimeDto elementoLista: listaDeTimes) {
            if (elementoLista.getNomeDoTime().equals(nome)) {
                return elementoLista;
            }
        }

        throw  new RuntimeException("Time não encontrado");
    }

}

