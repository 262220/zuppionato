package br.com.zup.Zuppionato.services;

import br.com.zup.Zuppionato.dtos.TimeDto;
import br.com.zup.Zuppionato.dtos.TreinadorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TreinadorService {
private List <TreinadorDto> listaDeTreinadores = new ArrayList<>();

@Autowired
    TimeService timeService;

public TreinadorDto cadastrarTreinador (String nome, TreinadorDto novoTreinador) {
    TimeDto timeDoTreinador = timeService.pesquisarTimePorNome(nome);

    listaDeTreinadores.add(novoTreinador);
    timeDoTreinador.setTreinador(novoTreinador);

    return novoTreinador;
}

public TreinadorDto pesquisarTreinadorPorCpf (String cpf) {
for (TreinadorDto elementoLista: listaDeTreinadores) {
    if (elementoLista.getCpf().equals(cpf)) {
        return elementoLista;
    }
}
throw  new RuntimeException("Treinador não encontrado");
}
}

