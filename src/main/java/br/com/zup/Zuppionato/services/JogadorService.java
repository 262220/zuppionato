package br.com.zup.Zuppionato.services;

import br.com.zup.Zuppionato.dtos.JogadorDto;
import br.com.zup.Zuppionato.dtos.TimeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JogadorService {
    @Autowired
    TimeService timeService;

    public JogadorDto cadastrarJogadorNoTime(String nome, JogadorDto jogadorDto){
TimeDto timeJogador = timeService.pesquisarTimePorNome(nome);

        timeJogador.getListaDeJogadores().add(jogadorDto);
        return jogadorDto;
    }

    public JogadorDto pesquisarJogadorPorCpf (String nome, String cpf) {
        TimeDto timeJogador = timeService.pesquisarTimePorNome(nome);

        for (JogadorDto elementoLista: timeJogador.getListaDeJogadores()) {
            if (elementoLista.getCpf().equals(cpf)) {
                return elementoLista;
            }
        }

    throw  new RuntimeException("Jogador não encontrado");
    }
}

