package br.com.zup.Zuppionato.controllers;

import br.com.zup.Zuppionato.dtos.JogadorDto;
import br.com.zup.Zuppionato.services.JogadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/jogador")
public class JogadorController {
    @Autowired
    private JogadorService jogadorService;

    @PostMapping("/{nome}")
    public JogadorDto adicionarJogadorEmUmTime (@PathVariable String nome, JogadorDto jogadorDto) {
        return jogadorService.cadastrarJogadorNoTime(nome, jogadorDto);
    }

    @GetMapping("/pesquisar/{nome}/{cpf}")
    public JogadorDto retornarJogadorPesquisado (@PathVariable String nome, @PathVariable String cpf) {
        return jogadorService.pesquisarJogadorPorCpf(nome, cpf);
    }

}

