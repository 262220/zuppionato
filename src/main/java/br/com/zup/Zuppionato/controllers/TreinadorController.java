package br.com.zup.Zuppionato.controllers;

import br.com.zup.Zuppionato.dtos.TreinadorDto;
import br.com.zup.Zuppionato.services.TreinadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/treinador")
public class TreinadorController {
    @Autowired
    TreinadorService treinadorService;

    @PostMapping("/{nome}")
    public TreinadorDto adicionarTreinadorParaUmTime(@PathVariable String nome, @RequestBody TreinadorDto novoTreinador) {
        treinadorService.cadastrarTreinador(nome, novoTreinador);

        return novoTreinador;
    }

    @GetMapping("/pesquisar/{cpf}")
    public TreinadorDto retornarTreinadorPesquisado(@PathVariable String cpf) {
        return treinadorService.pesquisarTreinadorPorCpf(cpf);
    }

}

