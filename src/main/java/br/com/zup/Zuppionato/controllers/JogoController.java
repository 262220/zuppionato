package br.com.zup.Zuppionato.controllers;

import br.com.zup.Zuppionato.dtos.JogoDto;
import br.com.zup.Zuppionato.services.JogoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/jogos")
public class JogoController {
    @Autowired
    private JogoService jogoService;

    @PostMapping("/{nome1}/{nome2}")
    public JogoDto adicionarJogo(@PathVariable String nome1, @PathVariable String nome2, @RequestBody JogoDto novoJogo) {
        return jogoService.agendarJogo(nome1, nome2, novoJogo);
    }

    @GetMapping
    public List<JogoDto> mostrarCalendario() {
        return jogoService.mostrarCalendario();
    }
}

