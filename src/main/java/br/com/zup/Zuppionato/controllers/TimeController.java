package br.com.zup.Zuppionato.controllers;

import br.com.zup.Zuppionato.dtos.TimeDto;
import br.com.zup.Zuppionato.services.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/times")
public class TimeController {
@Autowired
    TimeService timeService;

@PostMapping
public TimeDto adicionarTime (@RequestBody TimeDto novoTime) {
    timeService.cadastrarTime(novoTime);

    return novoTime;
}

@GetMapping
public List <TimeDto> mostrarTimesDaLista () {
    return timeService.mostrarTimes();
}

@GetMapping("/pesquisar/{nome}")
public TimeDto retornarTimePesquisadoPeloNome (@PathVariable String nome) {

    return timeService.pesquisarTimePorNome(nome);
}
}

