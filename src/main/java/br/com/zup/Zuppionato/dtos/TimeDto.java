package br.com.zup.Zuppionato.dtos;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

public class TimeDto {
    @Size(min = 4, message = "{validacao.nomeTime}")
    private String nomeDoTime;

    @Size(min = 2, message = "{validacaoabreviacaoNome")
    private String abreviacaoNome;

    private String cores;
    private TreinadorDto treinador;
    private List <JogadorDto> listaDeJogadores = new ArrayList ();

    public TimeDto() {
    }

    public String getNomeDoTime() {
        return nomeDoTime;
    }

    public void setNomeDoTime(String nomeDoTime) {
        this.nomeDoTime = nomeDoTime;
    }

    public String getCores() {
        return cores;
    }

    public void setCores(String cores) {
        this.cores = cores;
    }

    public List<JogadorDto> getListaDeJogadores() {
        return listaDeJogadores;
    }

    public void setListaDeJogadores(List<JogadorDto> listaDeJogadores) {
        this.listaDeJogadores = listaDeJogadores;
    }

    public String getAbreviacaoNome() {
        return abreviacaoNome;
    }

    public void setAbreviacaoNome(String abreviacaoNome) {
        this.abreviacaoNome = abreviacaoNome;
    }

    public TreinadorDto getTreinador() {
        return treinador;
    }

    public void setTreinador(TreinadorDto treinador) {
        this.treinador = treinador;
    }
}
