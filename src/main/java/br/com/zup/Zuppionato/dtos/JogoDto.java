package br.com.zup.Zuppionato.dtos;

import java.time.LocalDate;

public class JogoDto {
private TimeDto timeMandante;
private TimeDto timeVisitante;
private String local;
private LocalDate data;

    public JogoDto() {
    }

    public TimeDto getTimeMandante() {
        return timeMandante;
    }

    public void setTimeMandante(TimeDto timeMandante) {
        this.timeMandante = timeMandante;
    }

    public TimeDto getTimeVisitante() {
        return timeVisitante;
    }

    public void setTimeVisitante(TimeDto timeVisitante) {
        this.timeVisitante = timeVisitante;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }
}

