package br.com.zup.Zuppionato.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public abstract class PessoaDto {
    @Size(min = 2, message = "{validacao.nome}")
    private String nome;

    @Size(min = 11, max = 11, message = "{validacao.cpf}")
    private String cpf;

    @Min(value = 15, message = "{validacao.idade}")
    private int idade;

    public PessoaDto(String nome, String cpf, int idade) {
        this.nome = nome;
        this.cpf = cpf;
        this.idade = idade;
    }

    public PessoaDto() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}

