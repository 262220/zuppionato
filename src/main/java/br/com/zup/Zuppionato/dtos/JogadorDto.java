package br.com.zup.Zuppionato.dtos;

import br.com.zup.Zuppionato.enums.PosicaoEnum;

import javax.validation.constraints.NotBlank;

public class JogadorDto extends PessoaDto {

    @NotBlank(message = "{validacao.numeroDaCamisa.obrigatorio}")
    private int numeroDaCamisa;

    @NotBlank(message = "{validacao.posicao.obrigatorio")
    private PosicaoEnum posicao;

    public JogadorDto() {
    }

    public int getNumeroDaCamisa() {
        return numeroDaCamisa;
    }

    public void setNumeroDaCamisa(int numeroDaCamisa) {
        this.numeroDaCamisa = numeroDaCamisa;
    }

    public PosicaoEnum getPosicao() {
        return posicao;
    }

    public void setPosicao(PosicaoEnum posicao) {
        this.posicao = posicao;
    }
}

