package br.com.zup.Zuppionato.dtos;

public class TreinadorDto extends PessoaDto {

    public TreinadorDto(String nome, String cpf, int idade) {
        super(nome, cpf, idade);
    }

    public TreinadorDto() {
    }
}

