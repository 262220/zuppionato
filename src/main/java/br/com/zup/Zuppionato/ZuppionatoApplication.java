package br.com.zup.Zuppionato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZuppionatoApplication {
    public static void main(String[] args) {
            SpringApplication.run(ZuppionatoApplication.class, args);
    }

}
